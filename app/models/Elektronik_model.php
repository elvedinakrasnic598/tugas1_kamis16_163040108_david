<?php

class Elektronik_model
{

	private $table = 'elektronik';
	private $db;

	public function __construct()
	{
		$this->db = new Database;
	}

	public function getAllElektronik()
	{
		$this->db->query("SELECT * FROM " . $this->table);
		return $this->db->resultSet();
	}

	public function getElektronikById($id)
	{

		$this->db->query("SELECT * FROM " . $this->table . " WHERE id=:id");
		$this->db->bind("id", $id);
		return $this->db->single();
	}

	//fungsi untuk menambah data ke db
	public function insert($tabel, $data = [])
	{
		$this->table = $tabel;
		$this->db->query("insert into ". $this->table ." ( gambar,nama_barang,merk,harga,deskripsi)
		values(:gambar,:nama_barang,:merk,:harga,:deskripsi)");
		$this->db->bind("gambar", $data["gambar"]);
		$this->db->bind("nama_barang", $data["nama_barang"]);
		$this->db->bind("merk", $data["merk"]);
		$this->db->bind("harga", $data["harga"]);
		$this->db->bind("deskripsi", $data["deskripsi"]);
		$this->db->execute();
	}

//fungsi untuk mendelete data base
	public function delete($table, $id)
	{
		$this->table = $table;
		$this->db->query("delete from ". $this->table .' where id=:id');
		$this->db->bind("id",$id);
		$this->db->execute();
	}

// fungsi untuk mengupdate data base64_decode
public function update($tabel, $data = []){
		$this->table = $tabel;
		$this->db->query("update ". $this->table ." set nama_barang=:nama_barang, merk=:merk, harga=:harga, gambar=:gambar, deskripsi=:deskripsi  where id=:id");
		$this->db->bind("nama_barang", $data["nama_barang"]);
		$this->db->bind("merk", $data["merk"]);
		$this->db->bind("harga", $data["harga"]);
		$this->db->bind("gambar",$data["gambar"]);
		$this->db->bind("deskripsi",$data["deskripsi"]);
		$this->db->bind("id", $data["id"]);
		$this->db->execute();
	}
//fungsi untuk serching
	public function cari($tabel, $data){
		$this->table = $tabel;
		$this->db->query("SELECT * FROM ". $this->table ." WHERE id=:cari OR nama_barang=:cari OR harga=:cari OR merk=:cari");
		$this->db->bind("cari",$data);
		return $this->db->resultSet();
	}

}
