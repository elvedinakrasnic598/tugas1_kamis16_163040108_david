<?php

class Belanja_model
{

	private $table = 'elektronik';
	private $db;

	public function __construct()
	{
		$this->db = new Database;
	}

	public function getAllBl()
	{
		$this->db->query("SELECT * FROM " . $this->table);
		return $this->db->resultSet();
	}

	public function getBlById($id)
	{
		$this->db->query("SELECT * FROM " . $this->table . " WHERE id=:id");
		$this->db->bind("id", $id);
		return $this->db->single();
	}

	public function cari2($tabel, $data){
		$this->table = $tabel;
		$this->db->query("SELECT * FROM ". $this->table ." WHERE id=:cari2 OR nama_barang=:cari2 OR harga=:cari2 OR merk=:cari2");
		$this->db->bind("cari2",$data);
		return $this->db->resultSet();
	}

}
