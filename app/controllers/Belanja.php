<?php

class Belanja extends Controller
{
	public function index()
	{
		$data = array(
			'judul' => "Daftar Belanja",
			'bl' => $this->model('Belanja_model')->getAllBl()
		);
		$this->view('templates/header', $data);
		$this->view('belanja/index', $data);
		$this->view('templates/footer');
	}

  public function detail($id)
  {
    $data = array(
      'judul' => "Detail Belanja",
      'bl' => $this->model('Belanja_model')->getBlById($id)
    );
    $this->view('templates/header', $data);
    $this->view('Belanja/detail', $data);
    $this->view('templates/footer');
  }

  public function cariglobal(){
    $global = $_POST["global"];
    if(isset($_POST["cari2"]))
    $data = array(
        'judul' => 'Daftar Elektronik',
        'bl'=>$this->model('Belanja_model')->cari2("global", $global)
        );
    $this->view('templates/header', $data);
    $this->view('belanja/index',$data);
    $this->view('templates/footer');
  }
}
