<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Halaman <?= $data['judul'] ?></title>
		<link rel="stylesheet" href="<?= BASEURL; ?>/css/bootstrap.css">
		<link rel="stylesheet" href="<?= BASEURL; ?>/css/animate.css">
			<link rel="icon" href="<?= BASEURL; ?>/img/favicon.png">
			<style media="screen">
				.iklan img{
					border-radius: 6px;
				}
				.t{
					font-family: sans-serif;
					font-weight: bold;
				}
				.com{
					font-size: 14px;
				}
				.update img{
					width:25px;
					height:25px;
				}
			</style>
	</head>
	<body>

		<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
			<div class="container">
				<a class="navbar-brand" href="<?= BASEURL ?>" >
					<img src="<?= BASEURL ?>/img/shop.svg" width="30" height="30" class="d-inline-block align-top" alt="">
						<span class="t">t</span>uku.<span class="com">com</span>
				</a>

		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>

		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav mr-auto">
		      <li class="nav-item active">
		        <a class="nav-link" href="<?= BASEURL ?>">Home <span class="sr-only">(current)</span></a>
		      </li>
					<li class="nav-item">
						<a class="nav-link " href="<?= BASEURL ?>/elektronik">CRUD</a>
					</li>
					<li class="nav-item">
						<a class="nav-link " href="<?= BASEURL ?>/belanja">Belanja</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?= BASEURL ?>/about">About</a>
					</li>
		    </ul>
		    <form class="form-inline my-2 my-lg-0" action="<?= BASEURL; ?>/Belanja/cariglobal" method="post">
		      <input class="form-control mr-sm-2" name="global" type="search" placeholder="Search" aria-label="Search">
		      <button class="btn btn-outline-light my-2 my-sm-0 " name="cari2" type="submit">Search</button>
		    </form>
		  </div>
		</div>
		</nav>

		<div class="container mt-5">
