		<div class="text-center mb-5">
			<b><h1>Update Data Elektronik</h1></b>
			<hr>
		</div>
		<form action="<?=BASEURL;?>/Elektronik/update" method="post">
				<input type="hidden" name="id" value="<?= $data['elk']['id'] ?>">
		  <div class="form-row">
		    <div class="form-group col-md-6">
		      <label for="inputEmail4">Nama Barang</label>
					<input type="text" class="form-control" name="nama_barang"   value="<?= $data['elk']['nama_barang'] ?>">
		    </div>
		    <div class="form-group col-md-6">
		      <label for="inputPassword4">Merk</label>
					<input type="text" class="form-control" name="merk"  value="<?= $data['elk']['merk'] ?>">
		    </div>
		  </div>
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="inputEmail4">Harga</label>
					<input type="text" class="form-control" name="harga"   value="<?= $data['elk']['harga'] ?>">
				</div>
				<div class="form-group col-md-6">
					<label for="inputPassword4">Gambar</label>
					<input type="text" class="form-control" name="gambar"  value="<?= $data['elk']['gambar'] ?>">
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-6">
			<textarea class="form-control" id="exampleFormControlTextarea1" name="deskripsi" rows="3"><?= $data['elk']['deskripsi'] ?></textarea>
				</div>
			</div>
		 <button type="submit" name="ubah"class="btn btn-primary">SUBMIT</button>

		 <a href="<?= BASEURL ?>/elektronik">
		 <button type="button" class="btn btn-danger">BATAL</button>
		 </a>
		</form>
